import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/components/jogym_components/ticket/components_pt_ticket.dart';
import 'package:jo_gym_app/components/jogym_components/ticket/components_season_ticket.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';

class PageCustomerBuyTicket extends StatefulWidget {
  const PageCustomerBuyTicket({super.key, required this.customerId});

  final int customerId;

  @override
  State<PageCustomerBuyTicket> createState() => _PageCustomerBuyTicketState();
}

// "address": "string",
// "dateBirth": "string",
// "gender": "MALE",
// "name": "string",
// "phoneNumber": "string"

class _PageCustomerBuyTicketState extends State<PageCustomerBuyTicket> {
  bool _isBtnSwitch = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "회원권 구매"),
      body: Column(
        children: [
          const ComponentMarginVertical(enumSize: EnumSize.mid),
          Container(
            padding: bodyPaddingLeftRightAndVerticalHalf,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: MediaQuery.sizeOf(context).width / 3,
                  child: (_isBtnSwitch)
                      ? ComponentTextBtn(
                          '정기권',
                          bgColor: Color.fromRGBO(49, 176, 121, 100),
                          borderColor: Colors.white,
                          () {},
                        )
                      : ComponentTextBtn('정기권',
                          bgColor: Colors.grey,
                          borderColor: Colors.white,
                          foregroundColor: Colors.white, () {
                          setState(() {
                            _isBtnSwitch = !_isBtnSwitch;
                          });
                        }),
                ),
                Container(
                  width: MediaQuery.sizeOf(context).width / 3,
                  child: (!_isBtnSwitch)
                      ? ComponentTextBtn(
                          'PT권',
                          bgColor: Color.fromRGBO(49, 176, 121, 100),
                          borderColor: Colors.white,
                          () {},
                        )
                      : ComponentTextBtn('PT권',
                          bgColor: Colors.grey,
                          borderColor: Colors.white,
                          foregroundColor: Colors.white, () {
                          setState(() {
                            _isBtnSwitch = !_isBtnSwitch;
                          });
                        }),
                ),
              ],
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              padding: bodyPaddingLeftRight,
              child: _isBtnSwitch
                  ? _setMockUpData_SeasonTicket()
                  : _setMockUpData_PtTicket(),
            ),
          ),
        ],
      ),
    );
  }


  Widget _setMockUpData_SeasonTicket() {
    return  Column(
          children: [
            ComponentSeasonTicket(
              number: 2,
              seasonTicketName: "3개월권",
              month: "3개월",
              monthPrice: "100,000원/월",
              totalPrice: "100,000원",
              profileImg: Image.asset("assets/ss.png"),
              voidCallback: () {
                print("profile : 2");
              },
            ),
          ],
        );
  }
  // "id": 0,
  // "maxCount": 0,
  // "ticketName": "string",
  // "totalPrice": 0,
  // "trainerMember": 0,
  // "unitPrice": 0
  Widget _setMockUpData_PtTicket() {
    return Column(
      children: [
        ComponentPtTicket(
          number: 1,
          ptTicketName: "PT",
          maxCount: 50.toString(),
          unitPrice: 10000.toString(),
          totalPrice: 1000000.toString(),
          voidCallback: () {
            print("profile : 1");
          },
        )
      ],
    );
  }

}
