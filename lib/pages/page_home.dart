import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_margin_horizon.dart';
import 'package:jo_gym_app/config/config_color.dart';
import 'package:jo_gym_app/config/config_size.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/functions/token_lib.dart';
import 'package:jo_gym_app/pages/customer/page_customer.dart';
import 'package:jo_gym_app/pages/page_test.dart';
import 'package:jo_gym_app/pages/pt/page_pt_ticket.dart';
import 'package:jo_gym_app/pages/season/page_season_ticket.dart';
import 'package:jo_gym_app/pages/trainer/page_trainer.dart';

class PageHome extends StatefulWidget {
  const PageHome({super.key});

  @override
  State<PageHome> createState() => _PageHomeState();
}

class _PageHomeState extends State<PageHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "메인 페이지"),
      body: Column(
        children: [
          Container(
            padding: bodyPaddingLeftRight,
            child: Center(
              child: Column(
                children: [
                  SizedBox(
                    height: 50,
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.44,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                primary: colorLightGray,
                                onPrimary: Colors.black,
                                padding: contentPaddingButton,
                                elevation: buttonElevation,
                                textStyle: const TextStyle(
                                  fontSize: fontSizeMid,
                                ),
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.circular(buttonRadius),
                                    side: const BorderSide(
                                      color: colorLightGray,
                                    ))),
                            child: const Padding(
                              padding: EdgeInsets.only(top: 15, bottom: 15),
                              child: Text(
                                "회원",
                                style: TextStyle(fontSize: fontSizeSuper),
                              ),
                            ),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          PageCustomer()));
                            },
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.44,
                          child: ElevatedButton(
                            onPressed: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        PageTrainer())),
                            style: ElevatedButton.styleFrom(
                                primary: colorLightGray,
                                onPrimary: Colors.black,
                                padding: contentPaddingButton,
                                elevation: buttonElevation,
                                textStyle: const TextStyle(
                                  fontSize: fontSizeMid,
                                ),
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.circular(buttonRadius),
                                    side: const BorderSide(
                                      color: colorLightGray,
                                    ))),
                            child: const Padding(
                              padding: EdgeInsets.only(top: 15, bottom: 15),
                              child: Text(
                                "트레이너",
                                style: TextStyle(fontSize: fontSizeSuper),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.44,
                          child: ElevatedButton(
                            onPressed: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        const PageSeasonTicket())),
                            style: ElevatedButton.styleFrom(
                                primary: colorLightGray,
                                onPrimary: Colors.black,
                                padding: contentPaddingButton,
                                elevation: buttonElevation,
                                textStyle: const TextStyle(
                                  fontSize: fontSizeMid,
                                ),
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.circular(buttonRadius),
                                    side: const BorderSide(
                                      color: colorLightGray,
                                    ))),
                            child: const Padding(
                              padding: EdgeInsets.only(top: 15, bottom: 15),
                              child: Text(
                                "정기권",
                                style: TextStyle(fontSize: fontSizeSuper),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.44,
                          child: ElevatedButton(
                            onPressed: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        PagePtTicket())),
                            style: ElevatedButton.styleFrom(
                                primary: colorLightGray,
                                onPrimary: Colors.black,
                                padding: contentPaddingButton,
                                elevation: buttonElevation,
                                textStyle: const TextStyle(
                                  fontSize: fontSizeMid,
                                ),
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.circular(buttonRadius),
                                    side: const BorderSide(
                                      color: colorLightGray,
                                    ))),
                            child: const Padding(
                              padding: EdgeInsets.only(top: 15, bottom: 15),
                              child: Text(
                                "PT권",
                                style: TextStyle(fontSize: fontSizeSuper),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ), // PT권, 정기권
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: ElevatedButton(
                      onPressed: () {},
                      style: ElevatedButton.styleFrom(
                          primary: colorLightGray,
                          onPrimary: Colors.black,
                          padding: contentPaddingButton,
                          elevation: buttonElevation,
                          textStyle: const TextStyle(
                            fontSize: fontSizeMid,
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(buttonRadius),
                              side: const BorderSide(
                                color: colorLightGray,
                              ))),
                      child: const Padding(
                        padding: EdgeInsets.only(top: 15, bottom: 15),
                        child: Text(
                          "정산",
                          style: TextStyle(fontSize: fontSizeSuper),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: colorLightGray,
                          onPrimary: Colors.black,
                          padding: contentPaddingButton,
                          elevation: buttonElevation,
                          textStyle: const TextStyle(
                            fontSize: fontSizeMid,
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(buttonRadius),
                              side: const BorderSide(
                                color: colorLightGray,
                              ))),
                      child: const Padding(
                        padding: EdgeInsets.only(top: 15, bottom: 15),
                        child: Text(
                          "로그아웃",
                          style: TextStyle(fontSize: fontSizeSuper),
                        ),
                      ),
                      onPressed: () {
                        _ShowDialog(
                          titleText: "로그아웃",
                          titleColor: colorRed,
                          contentText: "정말 로그아웃을 하시겠습니까?",
                          contentColor: colorDarkGray,
                          callbackOk: () {
                            TokenLib.logout(context);
                          },
                          callbackCancel: () {
                            Navigator.pop(context);
                          },
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  _ShowDialog({
    required String titleText,
    required Color titleColor,
    required String contentText,
    required Color contentColor,
    required VoidCallback callbackOk,
    required VoidCallback callbackCancel,
  }) {
    showDialog(
        context: context,
        //barrierDismissible - Dialog를 제외한 다른 화면 터치 x
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            // RoundedRectangleBorder - Dialog 화면 모서리 둥글게 조절
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            //Dialog Main Title
            title: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(Icons.report_problem_outlined, color: titleColor),
                ComponentMarginHorizon(enumSize: EnumSize.mid),
                Text(titleText,
                    style: TextStyle(
                        color: titleColor,
                        fontSize: fontSizeBig,
                        fontWeight: FontWeight.bold)),
              ],
            ),
            content: Text(contentText,
                style: TextStyle(
                    color: contentColor,
                    fontSize: fontSizeMid,
                    fontWeight: FontWeight.bold)),
            actions: [
              TextButton(
                child: Text("확인"),
                onPressed: callbackOk,
              ),
              TextButton(
                child: Text("취소"),
                onPressed: callbackCancel,
              ),
            ],
          );
        });
  }
}
