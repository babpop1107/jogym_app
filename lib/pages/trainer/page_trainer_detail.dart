import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_custom_loading.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_notification.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/components/component_divider.dart';
import 'package:jo_gym_app/config/config_form_formatter.dart';
import 'package:jo_gym_app/config/config_form_validator.dart';
import 'package:jo_gym_app/config/config_size.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/model/trainer/trainer_update_request.dart';
import 'package:jo_gym_app/repository/trainer/repo_trainer.dart';
import 'package:jo_gym_app/styles/style_form_decoration_full_going.dart';

class PageTrainerDetail extends StatefulWidget {
  const PageTrainerDetail({super.key, required this.trainerId});

  final int trainerId;

  @override
  State<PageTrainerDetail> createState() => _PageTrainerDetailState();
}

class _PageTrainerDetailState extends State<PageTrainerDetail> {
  final _formKey = GlobalKey<FormBuilderState>();
  final _dropDownKey = GlobalKey<FormBuilderFieldState>();
  final TextEditingController _textControllerOfName = TextEditingController();
  final TextEditingController _textControllerOfPhoneNumber =
  TextEditingController();
  final TextEditingController _textControllerOfAddress =
  TextEditingController();
  final TextEditingController _textControllerOfCareerContent =
  TextEditingController();

  final TextEditingController _textControllerOfMemo = TextEditingController();

  String _strDateCreate = "2023-04-12";
  String _initValueOfGender = "";
  DateTime _initDateBirth = DateTime(2023, 01, 01);
  bool _isFixEnable = false;

  Future<void> _getTrainer(int customerId) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoTrainer().getData(trainerMemberId: customerId).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _textControllerOfName.text = res.data.name;
        _textControllerOfPhoneNumber.text = res.data.phoneNumber;
        _textControllerOfAddress.text = res.data.address;
        _initDateBirth = DateTime.parse(res.data.dateBirth);
        _textControllerOfMemo.text = res.data.memo ?? "";
        _dropDownKey.currentState!.setValue(res.data.gender);
        _strDateCreate =
            res.data.dateCreate.substring(0, res.data.dateCreate.indexOf(" "));

        // print("customerId : ${widget.customerId}");
        // print("dateCreate : ${res.data.dateCreate}");
        // print("name : ${_textControllerOfName.text}");
        // print("phoneNumber : ${_textControllerOfPhoneNumber.text}");
        // print("address : ${_textControllerOfAddress.text}");
        // print("gender : ${res.data.gender}");
        // print("name : $_initDateBirth");
        // print("memo : ${_textControllerOfMemo.text}");
      });

      ComponentNotification(
        success: true,
        title: '직원정보 불러오기 완료',
        subTitle: '직원정보 불러오기가 완료되었습니다.',
      ).call();
    }).catchError((err) {
      BotToast.closeAllLoading();

      print(err.toString());

      ComponentNotification(
        success: false,
        title: '직원정보 불러오기 실패',
        subTitle: '확인해주세요.',
      ).call();
    });
  }


  Future<void> _putTrainer(int trainerId, TrainerUpdateRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoTrainer()
        .putData(trainerMemberId: trainerId, request: request)
        .then((res) {
      BotToast.closeAllLoading();
      setState(() {
        _isFixEnable = false;
      });
      ComponentNotification(
        success: true,
        title: '직원정보 저장 완료',
        subTitle: '직원정보 저장이 완료되었습니다.',
      ).call();
    }).catchError((err) {
      BotToast.closeAllLoading();
      ComponentNotification(
        success: false,
        title: '직원정보 저장 실패',
        subTitle: '확인해주세요.',
      ).call();
    });
  }



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getTrainer(widget.trainerId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: '직원 상세 정보 관리'),
      body: SingleChildScrollView(
        padding: bodyPaddingLeftRight,
        child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const ComponentMarginVertical(enumSize: EnumSize.mid),
                Container(
                  alignment: Alignment.centerLeft,
                  width: 200,
                  height: 200,
                  child: Image.asset("assets/ulgen.png"),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.mid),
                Text("등록일 : $_strDateCreate",
                    style: TextStyle(
                        fontSize: fontSizeBig, fontWeight: FontWeight.w500)),
                const ComponentMarginVertical(),
                ComponentDivider(),
                const ComponentMarginVertical(),
                FormBuilderTextField(
                  name: 'name',
                  controller: _textControllerOfName,
                  enabled: _isFixEnable,
                  decoration: StyleFormDecorationOfInputText()
                      .getInputDecoration('직원명'),
                  maxLength: 20,
                  keyboardType: TextInputType.text,
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                    FormBuilderValidators.minLength(3,
                        errorText: formErrorMinLength(3)),
                    FormBuilderValidators.maxLength(20,
                        errorText: formErrorMaxLength(20)),
                  ]),
                ),
                const ComponentMarginVertical(
                  enumSize: EnumSize.micro,
                ),
                FormBuilderTextField(
                  name: 'phoneNumber',
                  controller: _textControllerOfPhoneNumber,
                  enabled: _isFixEnable,
                  decoration: StyleFormDecorationOfInputText()
                      .getInputDecoration('연락처'),
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    maskPhoneNumberInputFormatter
                    //13자리만 입력받도록 하이픈 2개+숫자 11개
                  ],
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                    FormBuilderValidators.minLength(13,
                        errorText: formErrorMinLength(13)),
                    FormBuilderValidators.maxLength(13,
                        errorText: formErrorMaxLength(13)),
                  ]),
                ),
                const ComponentMarginVertical(
                  enumSize: EnumSize.micro,
                ),
                FormBuilderTextField(
                  name: 'address',
                  controller: _textControllerOfAddress,
                  enabled: _isFixEnable,
                  decoration:
                  StyleFormDecorationOfInputText().getInputDecoration('주소'),
                  keyboardType: TextInputType.text,
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                    FormBuilderValidators.minLength(3,
                        errorText: formErrorMinLength(3)),
                    FormBuilderValidators.maxLength(100,
                        errorText: formErrorMaxLength(100)),
                  ]),
                ),
                const ComponentMarginVertical(
                  enumSize: EnumSize.micro,
                ),
                FormBuilderDropdown<String>(
                  name: 'gender',
                  key: _dropDownKey,
                  enabled: _isFixEnable,
                  initialValue: _initValueOfGender,
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                  ]),
                  decoration: const InputDecoration(
                    labelText: '성별',
                    filled: true,
                    isDense: false,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                    ),
                  ),
                  items: const [
                    DropdownMenuItem(value: '남자', child: Text('남자')),
                    DropdownMenuItem(value: '여자', child: Text('여자')),
                  ],
                ),
                const ComponentMarginVertical(enumSize: EnumSize.small),
                FormBuilderDateTimePicker(
                  name: 'dateBirth',
                  initialEntryMode: DatePickerEntryMode.calendarOnly,
                  initialValue: _initDateBirth,
                  inputType: InputType.date,
                  firstDate: DateTime(1970),
                  lastDate: DateTime(2030),
                  format: DateFormat('yyyy-MM-dd'),
                  enabled: _isFixEnable,
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                  ]),
                  decoration: const InputDecoration(
                    labelText: '생년월일',
                    filled: true,
                    isDense: false,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                    ),
                  ),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.small),
                FormBuilderTextField(
                  name: 'careerContent',
                  controller: _textControllerOfCareerContent,
                  enabled: _isFixEnable,
                  keyboardType: TextInputType.multiline,
                  maxLines: 4,
                  decoration: StyleFormDecorationOfInputText()
                      .getInputDecoration('수상내역'),
                ),
                FormBuilderTextField(
                  name: 'memo',
                  controller: _textControllerOfMemo,
                  enabled: _isFixEnable,
                  keyboardType: TextInputType.multiline,
                  maxLines: 4,
                  decoration:
                  StyleFormDecorationOfInputText().getInputDecoration('비고'),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.mid),
                Container(
                  width: MediaQuery.sizeOf(context).width,
                  child: _isFixEnable
                      ? ComponentTextBtn(
                    "저장",
                    bgColor: Color.fromRGBO(84, 89, 87, 100),
                    borderColor: Colors.white,
                        () {
                      if (_formKey.currentState!.validate()) {
                        String gender = "";
                        switch (_formKey
                            .currentState!.fields['gender']!.value) {
                          case "남자":
                            gender = "MALE";
                            break;
                          case "여자":
                            gender = "FEMALE";
                            break;
                        }

                        TrainerUpdateRequest request =
                        TrainerUpdateRequest(
                          _formKey.currentState!.fields['name']!.value,
                          _formKey.currentState!.fields['phoneNumber']!.value,
                          _formKey.currentState!.fields['address']!.value,
                          gender,
                          DateFormat('yyyy-MM-dd').format(_formKey.currentState!.fields['dateBirth']!.value),
                          _formKey.currentState!.fields['careerContent']!.value,
                          _formKey.currentState!.fields['memo']!.value,

                        );

                        _putTrainer(widget.trainerId, request);


                      }
                    },
                  )
                      : ComponentTextBtn(
                    "수정",
                    bgColor: Color.fromRGBO(84, 89, 87, 100),
                    borderColor: Colors.white,
                        () {
                      setState(() {
                        _isFixEnable = true;
                      });
                    },
                  ),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.micro),
                Container(
                  width: MediaQuery.sizeOf(context).width,
                  child: ComponentTextBtn(
                    '탈퇴',
                    bgColor: Color.fromRGBO(84, 89, 87, 100),
                    borderColor: Colors.white,
                        () {},
                  ),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.small),
              ],
            )),
      ),
    );
  }
}
