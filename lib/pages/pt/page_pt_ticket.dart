import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_custom_loading.dart';
import 'package:jo_gym_app/components/common/component_no_contents.dart';
import 'package:jo_gym_app/components/jogym_components/card/component_customer_card.dart';
import 'package:jo_gym_app/components/jogym_components/ticket/components_pt_ticket.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/model/customer/customer_item.dart';
import 'package:jo_gym_app/model/pt/pt_ticket_item.dart';
import 'package:jo_gym_app/pages/customer/page_customer_detail.dart';
import 'package:jo_gym_app/pages/pt/page_pt_ticket_change.dart';
import 'package:jo_gym_app/pages/pt/page_pt_ticket_create.dart';
import 'package:jo_gym_app/repository/customer/repo_customer.dart';
import 'package:jo_gym_app/repository/repo_pt_ticket.dart';


class PagePtTicket extends StatefulWidget {
  const PagePtTicket({super.key});

  @override
  State<PagePtTicket> createState() => _PageCustomerState();
}

class _PageCustomerState extends State<PagePtTicket> {
  final TextEditingController _searchTextController = TextEditingController();
  final _scrollController = ScrollController();

  List<PtTicketItem> _list = [];
  int _totalItemCount = 0;
  int _totalPage = 1;
  int _currentPage = 1;

  Future<void> _loadItems({bool reFresh = false}) async {
    // 새로고침하면 초기화시키기.
    if (reFresh) {
      _list = [];
      _totalItemCount = 0;
      _totalPage = 1;
      _currentPage = 1;
    }

    if (_currentPage <= _totalPage) {
      BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
        return ComponentCustomLoading(
          cancelFunc: cancelFunc,
        );
      });

      await RepoPtTicket()
          .getList(page: _currentPage)
          .then((res) => {
        BotToast.closeAllLoading(),
        setState(() {
          _totalItemCount = res.totalItemCount;
          _totalPage = res.totalPage;
          _list = [
            ..._list,
            ...?res.list
          ]; // 기존 리스트에 api에서 받아온 리스트 데이터를 더하는거.
          _currentPage++;
        })
      })
          .catchError((err) => {
        BotToast.closeAllLoading(),
        print(err),
      });
    }

    // 새로고침하면 스크롤위치를 맨 위로 올리기.
    if (reFresh) {
      _scrollController.animateTo(
        0,
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeOut,
      );
    }
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.offset ==
          _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });
    _loadItems();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: const ComponentAppbarPopup(title: "PT권 정보 관리"),
        floatingActionButton: FloatingActionButton(
          heroTag: "btn1",
          onPressed: () {
            _loadItems(reFresh: true);
          },
          child: const Icon(Icons.refresh),
        ),
        body: Column(
          children: [
            Container(
              padding: bodyPaddingLeftRightAndVerticalHalf,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      child: Flexible(
                        child: TextField(
                          controller: _searchTextController,
                          decoration: InputDecoration(
                            hintText: 'Search Name',
                            // Add a clear button to the search bar
                            suffixIcon: IconButton(
                              icon: const Icon(Icons.clear),
                              onPressed: () {
                                _searchTextController.clear();
                                _loadItems(reFresh: true);
                              },
                            ),
                            // Add a search icon or button to the search bar
                            prefixIcon: IconButton(
                              icon: const Icon(Icons.search),
                              onPressed: () {
                                // Perform the search here
                                print("Search Name :${_searchTextController.text}");
                                setState(() {
                                  _list = List.from(_list.where((element) {
                                    return (element.ticketName.compareTo(
                                        _searchTextController.text) ==
                                        0);
                                  }));
                                });
                              },
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                          ),
                        ),
                      )), // 회원검색
                  const SizedBox(width: 100),
                  FloatingActionButton(
                    heroTag: "btn2",
                    backgroundColor: Colors.blue,
                    tooltip: 'PT권 등록',
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                              const PagePtTicketCreate()));
                    },
                    child: Icon(
                      Icons.add,
                      size: 25,
                      color: Colors.white,
                    ),
                  ), // 신규회원등록
                ],
              ), //신규 회원 등록 박스
            ), //검색, 신규회원등록
            Expanded(
              child: SingleChildScrollView(
                controller: _scrollController,
                padding: bodyPaddingLeftRight,
                child: _buildBody(),
              ),
            ),
          ],
        ));
  }

  Widget _buildBody() {
    if (_totalItemCount > 0) {
      return Column(
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentPtTicket(
              number: _list[index].id,
              ptTicketName: _list[index].ticketName,
              maxCount: _list[index].maxCount.toString(),
              unitPrice: _list[index].unitPrice.toString(),
              totalPrice: _list[index].totalPrice.toString(),
              voidCallback: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            PagePtTicketChange(
                              ptTicketId: _list[index].id,
                              maxCount: _list[index].maxCount,
                              unitPrice: _list[index].unitPrice,
                            )));
              },
              // profileImg: Image.asset("assets/roni.png"),
            ),
          ),
        ],
      );
    } else {
      // 이용내역이 없을때 흰화면 보이면 에러난것처럼 보이니까 이용내역 없다고 보여주기.
      return SizedBox(
        height: MediaQuery.of(context).size.height - 30 - 50 - 50 - 70,
        child: GestureDetector(
          child: const ComponentNoContents( // 없다는 창 띄워주는 Component
            icon: Icons.history,
            msg: '등록된 PT권이 없습니다.',
          ),
          onTap: () => _loadItems(reFresh: true),
        ),
      );
    }
  }
}
