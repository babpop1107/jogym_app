import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_custom_loading.dart';
import 'package:jo_gym_app/components/common/component_margin_horizon.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_notification.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_color.dart';
import 'package:jo_gym_app/config/config_form_formatter.dart';
import 'package:jo_gym_app/config/config_form_validator.dart';
import 'package:jo_gym_app/config/config_size.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/model/pt/pt_ticket_update_request.dart';
import 'package:jo_gym_app/pages/page_home.dart';
import 'package:jo_gym_app/repository/repo_pt_ticket.dart';
import 'package:jo_gym_app/styles/style_form_decoration.dart';

class PagePtTicketChange extends StatefulWidget {
  const PagePtTicketChange(
      {super.key,
      required this.ptTicketId,
      required this.maxCount,
      required this.unitPrice});

  final int ptTicketId;
  final int maxCount;
  final num unitPrice;

  @override
  State<PagePtTicketChange> createState() => _PagePtTicketChangeState();
}

class _PagePtTicketChangeState extends State<PagePtTicketChange> {

  _ShowDialog({
    required String titleText,
    required Color titleColor,
    required String contentText,
    required Color contentColor,
    required VoidCallback callbackOk,
    required VoidCallback callbackCancel,
  })
  {
    showDialog(
        context: context,
        //barrierDismissible - Dialog를 제외한 다른 화면 터치 x
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            // RoundedRectangleBorder - Dialog 화면 모서리 둥글게 조절
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            //Dialog Main Title
            title: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(Icons.report_problem_outlined, color: titleColor),
                ComponentMarginHorizon(enumSize: EnumSize.mid),
                Text(titleText,
                    style: TextStyle(
                        color: titleColor,
                        fontSize: fontSizeBig,
                        fontWeight: FontWeight.bold)),
              ],
            ),
            content: Text(contentText,
                style: TextStyle(
                    color: contentColor,
                    fontSize: fontSizeMid,
                    fontWeight: FontWeight.bold)),
            actions: [
              TextButton(
                child: Text("확인"),
                onPressed: callbackOk,
              ),
              TextButton(
                child: Text("취소"),
                onPressed: callbackCancel,
              ),
            ],
          );
        });
  }



  final _formKey = GlobalKey<FormBuilderState>();
  final TextEditingController _textControllerOfUnitPrice = TextEditingController();
  final TextEditingController _textControllerOfMaxMonth = TextEditingController();


  Future<void> _putPtTicket(
      int ptTicketId, PtTicketUpdateRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoPtTicket()
        .putData(ptTicketId: ptTicketId, request: request)
        .then((res) {
      BotToast.closeAllLoading();
      setState(() {
        _isFixEnable = false;
      });
      ComponentNotification(
        success: true,
        title: '회원정보 저장 완료',
        subTitle: '회원정보 저장이 완료되었습니다.',
      ).call();
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageHome()),
              (route) => false);
    }).catchError((err) {
      BotToast.closeAllLoading();
      ComponentNotification(
        success: false,
        title: '회원정보 저장 실패',
        subTitle: '확인해주세요.',
      ).call();
    });
  }

  Future<void> _putPtTicketDelete() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoPtTicket()
        .doPtTicketDelete(widget.ptTicketId)
        .then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: 'PT권 삭제 완료',
        subTitle: 'PT권 삭제 완료 되었습니다.',
      ).call();
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageHome()),
              (route) => false);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: 'PT권 삭제 실패',
        subTitle: '문의처에 연락해주세요.',
      ).call();
    });
  }


  bool _isFixEnable = false;

  @override
  void initState() {
    super.initState();


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: 'PT권 정보 수정'),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Container(
                padding: bodyPaddingLeftRight,
                child: FormBuilder(
                  key: _formKey,
                  autovalidateMode: AutovalidateMode.disabled, // 자동 유효성 검사 비활성화
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'maxCount',
                          enabled: _isFixEnable,
                          decoration:
                              StyleFormDecoration().getInputDecoration('총 횟수'),
                          // maxLength: 20,
                          controller: _textControllerOfMaxMonth,
                          keyboardType: TextInputType.number,
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                            // FormBuilderValidators.minLength(1,
                            //     errorText: formErrorMinLength(1)),
                            // FormBuilderValidators.maxLength(20,
                            //     errorText: formErrorMaxLength(20)),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'unitPrice',
                          enabled: _isFixEnable,
                          controller: _textControllerOfUnitPrice,
                          decoration: StyleFormDecoration()
                              .getInputDecoration('요금 / 횟수'),
                          keyboardType: TextInputType.number,
                          inputFormatters: [MaskWonInputFormatter()],
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(enumSize: EnumSize.mid),
                      Container(
                        padding: bodyPaddingLeftRight,
                        child: _isFixEnable
                            ? ComponentTextBtn(
                          "저장",
                          bgColor: Color.fromRGBO(49, 176, 121, 100),
                          borderColor: Colors.white,
                              () {
                            if (_formKey.currentState!.validate()) {

                              print(
                                  "maxMonth : ${_formKey.currentState!.fields['maxCount']!.value}");
                              print(
                                  "unitPrice : ${_formKey.currentState!.fields['unitPrice']!.value}");

                              String tempUnitPrice = _formKey.currentState!.fields['unitPrice']!.value;
                              // String값 받아서 Replace all로 ,를 "" 빈 값으로 지정
                              PtTicketUpdateRequest request =
                              PtTicketUpdateRequest(
                                  int.parse(_formKey.currentState!.fields['maxCount']!.value),
                                num.parse(tempUnitPrice.replaceAll(',', "")),
                                // double은 소수점이 표기되어 ,가 들어갈 수 없기 때문에, 제거 후 넣어줌
                              );
                              _putPtTicket(widget.ptTicketId, request);
                            }
                          },
                        )
                            : ComponentTextBtn(
                          "수정",
                          bgColor: Color.fromRGBO(49, 176, 121, 100),
                          borderColor: Colors.white,
                              () {
                            setState(() {
                              _isFixEnable = true;
                            });
                          },
                        ),
                      ),
                      const ComponentMarginVertical(),
                  Container(
                    padding: bodyPaddingLeftRight,
                    child: ComponentTextBtn(
                      '삭제',
                          () {
                        _ShowDialog(
                            titleText: "PT권 삭제",
                            titleColor: colorRed,
                            contentText: "정말 삭제 하시겠습니까?",
                            contentColor: colorDarkGray,
                            callbackOk: (){_putPtTicketDelete();}, // 확인 누르면 해당 게시물 삭제
                            callbackCancel: (){Navigator.pop(context);} // 취소 누르면 전 화면 보이기
                        );
                      },
                      bgColor: Color.fromRGBO(84, 89, 87, 100),
                      borderColor: Colors.white,
                    ),
                  ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
