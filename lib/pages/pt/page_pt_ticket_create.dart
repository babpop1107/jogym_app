import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_custom_loading.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_notification.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_form_formatter.dart';
import 'package:jo_gym_app/config/config_form_validator.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/model/pt/pt_ticket_create_request.dart';
import 'package:jo_gym_app/pages/page_home.dart';
import 'package:jo_gym_app/pages/pt/page_pt_ticket.dart';
import 'package:jo_gym_app/pages/pt/page_pt_ticket_select_trainer_list.dart';
import 'package:jo_gym_app/repository/repo_pt_ticket.dart';
import 'package:jo_gym_app/styles/style_form_decoration.dart';

class PagePtTicketCreate extends StatefulWidget {
  const PagePtTicketCreate({super.key});

  @override
  State<PagePtTicketCreate> createState() => _PagePtTicketCreateState();
}

class _PagePtTicketCreateState extends State<PagePtTicketCreate> {
  final _formKey = GlobalKey<FormBuilderState>();
  final TextEditingController _textControllerOfTrainerData =
  TextEditingController();
  TrainerData trainerData = TrainerData(id: 0, name: "", isSave: false);

  Future<void> _setPtTicket(
      int trainerId, PtTicketCreateRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoPtTicket()
        .doCreate(trainerId: trainerId, request: request)
        .then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: 'PT권 등록 완료',
        subTitle: 'PT권 등록이 완료되었습니다.',
      ).call();
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PagePtTicket()),
              (route) => false);
    }).catchError((err) {
      print(err.toString());

      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: 'PT권 등록 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }

  @override
  void initState() {
    super.initState();
  }

  // "trainerId"
  // "maxMonth": 0,
  // "ticketName": "string",
  // "unitPrice": 0

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: 'Pt 정보 등록'),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Container(
                padding: bodyPaddingLeftRight,
                child: FormBuilder(
                  key: _formKey,
                  autovalidateMode: AutovalidateMode.disabled, // 자동 유효성 검사 비활성화
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'ticketName',
                          decoration:
                          StyleFormDecoration().getInputDecoration('PT권명'),
                          maxLength: 20,
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                            FormBuilderValidators.minLength(2,
                                errorText: formErrorMinLength(2)),
                            FormBuilderValidators.maxLength(20,
                                errorText: formErrorMaxLength(20)),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(),
                      GestureDetector(
                        child: Container(
                          decoration: formBoxDecoration,
                          padding: bodyPaddingAll,
                          child: FormBuilderTextField(
                            name: 'trainerName',
                            controller: _textControllerOfTrainerData,
                            decoration: StyleFormDecoration()
                                .getInputDecoration('트레이너'),
                            readOnly: true,
                            validator: FormBuilderValidators.compose([
                              FormBuilderValidators.required(
                                  errorText: formErrorRequired),
                            ]),
                            onTap: () async {
                              final selectData = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          PagePtTicketSelectTrainerList()));

                              setState(() {
                                if (selectData != null) {
                                  trainerData = selectData;
                                  _textControllerOfTrainerData.text =
                                      trainerData.name;
                                }
                              });
                            },
                          ),
                        ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'maxMonth',
                          decoration:
                          StyleFormDecoration().getInputDecoration('총 횟수'),
                          maxLength: 20,
                          keyboardType: TextInputType.text,
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                            FormBuilderValidators.minLength(1,
                                errorText: formErrorMinLength(1)),
                            FormBuilderValidators.maxLength(20,
                                errorText: formErrorMaxLength(20)),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'unitPrice',
                          decoration: StyleFormDecoration()
                              .getInputDecoration('요금 / 횟수'),
                          keyboardType: TextInputType.number,
                          inputFormatters: [MaskWonInputFormatter()],
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(enumSize: EnumSize.mid),
                      Container(
                        padding: bodyPaddingLeftRight,
                        child: ComponentTextBtn(
                          '저장',
                              () {
                            // "trainerId"
                            //  "maxMonth": 0,
                            // "ticketName": "string",
                            // "unitPrice": 0

                            if (_formKey.currentState!.saveAndValidate()) {
                              String strUnitPrice = _formKey
                                  .currentState!.fields['unitPrice']!.value;
                              strUnitPrice = strUnitPrice.replaceAll(',', "");
                              PtTicketCreateRequest request =
                              PtTicketCreateRequest(
                                  _formKey.currentState!
                                      .fields['ticketName']!.value,
                                  int.parse(_formKey.currentState!
                                      .fields['maxMonth']!.value),
                                  double.parse(strUnitPrice));

                              print("trainerId : ${trainerData.id}");
                              print(
                                  "ticketName : ${_formKey.currentState!.fields['ticketName']!.value}");
                              print(
                                  "maxCount : ${_formKey.currentState!.fields['maxMonth']!.value}");
                              print("unitPrice : ${strUnitPrice}");

                              _setPtTicket(trainerData.id, request);
                            }
                          },
                          bgColor: Color.fromRGBO(49, 176, 121, 100),
                          borderColor: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
