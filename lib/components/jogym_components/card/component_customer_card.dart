import 'package:flutter/material.dart';
import 'package:jo_gym_app/config/config_size.dart';

class ComponentCustomerCard extends StatelessWidget {
  const ComponentCustomerCard({super.key,
      required this.number, // 얘네는 필수
      required this.createDate,
      required this.name,
      required this.phoneNumber,
      this.profileImg,
      required this.voidCallback // 얘네는 필수
      });

  final int number; // No
  final String createDate; // 등록일
  final String name; // 회원명
  final String phoneNumber; // 연락처
  final Image? profileImg;
  final VoidCallback voidCallback; // 야 클릭됐다~~ 니가 처리해~~ 신호주기

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: voidCallback,
      child: Container(
          padding: const EdgeInsets.all(10),
          margin: const EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black, width: 2),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                width: 200,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const SizedBox(
                          width: 80,
                          child: Text(
                            "No",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          // margin: EdgeInsets.only(right: 10),
                        ),
                        Text(
                          "$number",
                          style: const TextStyle(
                            fontSize: fontSizeMid,
                            fontWeight: FontWeight.w400,
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const SizedBox(
                          width: 80,
                          child: Text(
                            "등록일",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          // margin: EdgeInsets.only(right: 10),
                        ),
                        Text(
                          createDate,
                          style: const TextStyle(
                            fontSize: fontSizeMid,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const SizedBox(
                          width: 80,
                          child: Text(
                            "회원명",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            name,
                            style: const TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const SizedBox(
                          width: 80,
                          child: Text(
                            "연락처",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Text(
                          phoneNumber,
                          style: const TextStyle(
                            fontSize: fontSizeMid,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 100,
                height: 100,
                child: profileImg,
              ),
            ],
          )),
    );
  }
}
