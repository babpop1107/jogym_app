import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jo_gym_app/config/config_size.dart';

class ComponentPtTicket extends StatelessWidget {
  const ComponentPtTicket({super.key,
    required this.number,
    required this.ptTicketName,
    required this.maxCount,
    required this.unitPrice,
    required this.totalPrice,
    required this.voidCallback});

  final int number;
  final String ptTicketName;
  final String maxCount;
  final String unitPrice;
  final String totalPrice;
  final VoidCallback voidCallback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: voidCallback,
      child: Container(
          margin: EdgeInsets.only(top: 10),
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black, width: 2),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 200,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 80,
                          child: const Text(
                            "No",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),

                        ),
                        Container(
                          child: Text(
                            "$number",
                            style: const TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 80,
                          child: const Text(
                            "PT권명",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            ptTicketName,
                            style: const TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 80,
                          child: const Text(
                            "횟수",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            "$maxCount",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 80,
                          child: const Text(
                            "금액",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            "$unitPrice",
                            style: const TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                    (totalPrice != null) ? Row(
                      mainAxisAlignment:
                      MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 80,
                          child: const Text(
                            "총 금액",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            totalPrice,
                            style: const TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ) : Container(),
                  ],
                ),
              ),
            ],
          )),
    );
  }
}
