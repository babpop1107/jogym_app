import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jo_gym_app/config/config_size.dart';

class ComponentSeasonTicket extends StatelessWidget {
  const ComponentSeasonTicket({super.key,
    required this.number,
    required this.seasonTicketName,
    required this.month,
    required this.monthPrice,
    required this.totalPrice,
    required this.profileImg,
    required this.voidCallback});

  final int number;
  final String seasonTicketName;
  final String month;
  final String monthPrice;
  final String totalPrice;
  final Image profileImg;
  final VoidCallback voidCallback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: voidCallback,
      child: Container(
          margin: EdgeInsets.only(top: 10),
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black, width: 2),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 200,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 80,
                          child: const Text(
                            "No",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),

                        ),
                        Container(
                          child: Text(
                            "$number",
                            style: const TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 80,
                          child: const Text(
                            "정기권명",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            seasonTicketName,
                            style: const TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 80,
                          child: const Text(
                            "월",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            month,
                            style: const TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 80,
                          child: Text(
                            "월 금액",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            monthPrice,
                            style: const TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 80,
                          child: const Text(
                            "총 금액",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            totalPrice,
                            style: const TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                width: 100,
                height: 100,
                child: Stack(
                  children: [
                    Container(
                      child: profileImg,
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.black, width: 1),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      child: Text("정기권", textAlign: TextAlign.justify),
                    ),
                  ],
                ),
              )
            ],
          )),
    );
  }
}
