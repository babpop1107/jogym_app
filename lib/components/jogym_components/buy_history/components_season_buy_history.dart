import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_size.dart';

class ComponentSeasonBuyHistory extends StatelessWidget {
  const ComponentSeasonBuyHistory(
      {super.key,
      required this.number,
      required this.historyName,
      required this.startDate,
      required this.endDate,
      required this.lastDate,
      required this.leftDays,
      required this.isValid,
      required this.profileImg,
      required this.voidCallbackAttendance,
      required this.voidCallbackComplete});

  final int number;
  final String historyName;
  final String startDate;
  final String endDate;
  final String lastDate;
  final int leftDays;
  final bool isValid;
  final Image profileImg;
  final VoidCallback voidCallbackAttendance;
  final VoidCallback voidCallbackComplete;

  // "historyId": 0,
  // "historyName": "string",
  // "startDate": "string"
  // "endDate": "string",
  // "lastDate": "string",
  // "leftDays": 0,
  // "buyStatus": "VALID",

  //  일일권명
  //  시작일
  //  종료일
  //  잔여일수
  //  최근방문일

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 10),
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black, width: 2),
        ),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      _setRowText(subTitle: "No.", subTitleVal: "$number"),
                      _setRowText(subTitle: "일일권명", subTitleVal: historyName),
                      _setRowText(subTitle: "시작일", subTitleVal: startDate),
                      _setRowText(subTitle: "종료일", subTitleVal: endDate),
                      _setRowText(subTitle: "잔여일수", subTitleVal: '$leftDays'),
                      _setRowText(subTitle: "최근방문일", subTitleVal: lastDate),
                      SizedBox(height: 10)
                    ],
                  ),
                ),
                Container(width: 100, height: 100, child: profileImg),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: isValid
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              child: ComponentTextBtn(
                                '출석',
                                voidCallbackAttendance,
                                bgColor:
                                    const Color.fromRGBO(49, 176, 121, 100),
                                borderColor: Colors.white,
                              ),
                            ),
                            SizedBox(
                              width: 50,
                            ),
                            Container(
                                child: ComponentTextBtn(
                              '완료',
                              voidCallbackComplete,
                              bgColor: const Color.fromRGBO(49, 176, 121, 100),
                              borderColor: Colors.white,
                            )),
                          ],
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              child: ComponentTextBtn(
                                '출석',
                                () {},
                                bgColor: Colors.grey,
                                borderColor: Colors.white,
                              ),
                            ),
                            SizedBox(
                              width: 50,
                            ),
                            Container(
                                child: ComponentTextBtn(
                              '완료',
                              () {},
                              bgColor: Colors.grey,
                              borderColor: Colors.white,
                            )),
                          ],
                        ),
                ),
                Container(
                  child: isValid
                      ? Container(
                          padding: EdgeInsets.all(3),
                          width: 100,
                          child: Text('유효',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  fontSize: fontSizeMid)),
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Colors.white,
                                  style: BorderStyle.solid,
                                  width: 2),
                              color: const Color.fromRGBO(49, 176, 121, 100),
                              borderRadius: BorderRadius.circular(20)),
                        )
                      : Container(
                          padding: EdgeInsets.all(3),
                          width: 100,
                          child: Text('완료',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  fontSize: fontSizeMid)),
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Colors.white,
                                  style: BorderStyle.solid,
                                  width: 2),
                              color: Colors.grey,
                              borderRadius: BorderRadius.circular(20)),
                        ),
                )
              ],
            )
          ],
        ));
  }

  Widget _setRowText({required String subTitle, required String subTitleVal}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: 100,
          child: Text(
            subTitle,
            style: const TextStyle(
              fontSize: fontSizeMid,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        Container(
          child: Text(
            subTitleVal,
            style: const TextStyle(
              fontSize: fontSizeMid,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
      ],
    );
  }
}
