class TrainerCreateRequest {
  String name;
  String phoneNumber;
  String address;
  String gender;
  String dateBirth;
  String? careerContent;
  String? memo;

  TrainerCreateRequest(this.name, this.phoneNumber, this.address, this.gender, this.dateBirth, this.careerContent, this.memo);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['name'] = name;
    data['phoneNumber'] = phoneNumber;
    data['address'] = address;
    data['gender'] = gender;
    data['dateBirth'] = dateBirth;
    data['careerContent'] = careerContent;
    data['memo'] = memo;

    return data;
  }
}