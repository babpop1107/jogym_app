import 'package:jo_gym_app/model/trainer/trainer_item.dart';

class TrainerItemResult {
  int currentPage;
  int totalItemCount;
  int totalPage;
  List<TrainerItem>? list;


  TrainerItemResult(this.currentPage, this.totalItemCount, this.totalPage, {this.list});

  factory TrainerItemResult.fromJson(Map<String, dynamic> json) {
    return TrainerItemResult(
      json['currentPage'],
      json['totalItemCount'],
      json['totalPage'],
      list: json['list'] != null ? (json['list'] as List).map((e) => TrainerItem.fromJson(e)).toList() : [],
    );
  }
}
