class CustomerUpdateRequest {

  String name;
  String phoneNumber;
  String address;
  String gender;
  String dateBirth;
  String? memo;


  CustomerUpdateRequest(this.name, this.phoneNumber, this.address, this.gender, this.dateBirth, this.memo);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['name'] = name;
    data['phoneNumber'] = phoneNumber;
    data['address'] = address;
    data['gender'] = gender;
    data['dateBirth'] = dateBirth;
    data['memo'] = memo;
    return data;
  }
}


// {
// "address": "string",
// "dateBirth": "2020-01-01",
// "gender": "MALE",
// "memo": "",
// "name": "123",
// "phoneNumber": "010-1234-4444"
// }