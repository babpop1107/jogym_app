class CustomerCreateRequest {

  String name;
  String phoneNumber;
  String address;
  String gender;
  String dateBirth;

  // "address": "string",
  // "dateBirth": "string",
  // "gender": "MALE",
  // "name": "string",
  // "phoneNumber": "string"

  CustomerCreateRequest(this.name, this.phoneNumber, this.address, this.gender, this.dateBirth);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['name'] = name;
    data['phoneNumber'] = phoneNumber;
    data['address'] = address;
    data['gender'] = gender;
    data['dateBirth'] = dateBirth;

    return data;
  }
}