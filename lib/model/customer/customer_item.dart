class CustomerItem {
  int id;
  String dateCreate;
  String name;
  String phoneNumber;


  CustomerItem(this.id, this.dateCreate, this.name, this.phoneNumber);

  factory CustomerItem.fromJson(Map<String, dynamic> json) {
    return CustomerItem(
      json['id'],
      json['dateCreate'],
      json['name'],
      json['phoneNumber']
    );
  }
}
