import 'package:jo_gym_app/model/customer/customer_item.dart';

class CustomerItemResult {
  int currentPage;
  int totalItemCount;
  int totalPage;
  List<CustomerItem>? list;


  CustomerItemResult(this.currentPage, this.totalItemCount, this.totalPage, {this.list});

  factory CustomerItemResult.fromJson(Map<String, dynamic> json) {
    return CustomerItemResult(
        json['currentPage'],
        json['totalItemCount'],
        json['totalPage'],
        list: json['list'] != null ? (json['list'] as List).map((e) => CustomerItem.fromJson(e)).toList() : [],
    );
  }
}
