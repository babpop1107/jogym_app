class PtTicketUpdateRequest {

  int maxCount;
  num unitPrice;


  PtTicketUpdateRequest(this.maxCount, this.unitPrice);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['maxCount'] = maxCount;
    data['unitPrice'] = unitPrice;
    return data;
  }
}


// {
// "address": "string",
// "dateBirth": "2020-01-01",
// "gender": "MALE",
// "memo": "",
// "name": "123",
// "phoneNumber": "010-1234-4444"
// }