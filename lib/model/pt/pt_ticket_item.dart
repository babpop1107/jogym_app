class PtTicketItem{
  int id;
  int trainerId;
  String ticketName;
  int maxCount;
  double unitPrice;
  double totalPrice;

  PtTicketItem(this.id, this.trainerId, this.ticketName, this.maxCount, this.unitPrice, this.totalPrice);

  factory PtTicketItem.fromJson(Map<String, dynamic> json) {
    return PtTicketItem(
      json['id'],
      json['trainerId'],
      json['ticketName'],
      json['maxCount'],
      json['unitPrice'],
      json['totalPrice']
    );
  }
}

//       "id": 2,
//       "trainerId": 1,
//       "ticketName": "마이클타이슨",
//       "maxCount": 10,
//       "unitPrice": 1202121,
//       "totalPrice": 12021210