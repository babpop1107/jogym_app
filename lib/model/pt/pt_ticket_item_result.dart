import 'package:jo_gym_app/model/pt/pt_ticket_item.dart';

class PtTicketItemResult {
  int currentPage;
  int totalItemCount;
  int totalPage;
  List<PtTicketItem>? list;


  PtTicketItemResult(this.currentPage, this.totalItemCount, this.totalPage, {this.list});

  factory PtTicketItemResult.fromJson(Map<String, dynamic> json) {
    return PtTicketItemResult(
      json['currentPage'],
      json['totalItemCount'],
      json['totalPage'],
      list: json['list'] != null ? (json['list'] as List).map((e) => PtTicketItem.fromJson(e)).toList() : [],
    );
  }
}