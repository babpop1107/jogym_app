class CalculateItem{
  int calculateId;
  String dateCreateMonth;
  String dateCreateYear;
  String calculateStatus;

  CalculateItem(this.calculateId, this.dateCreateMonth, this.dateCreateYear, this.calculateStatus);

  factory CalculateItem.fromJson(Map<String, dynamic> json) {
    return CalculateItem(
        json['calculateId'],
        json['dateCreateMonth'],
        json['dateCreateYear'],
        json['calculateStatus'],
    );
  }
}