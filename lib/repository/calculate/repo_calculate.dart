import 'package:dio/dio.dart';
import 'package:jo_gym_app/functions/token_lib.dart';
import 'package:jo_gym_app/model/calculate/calculate_item_result.dart';

class RepoCalculate{
  Future<CalculateItemResult> getList({required int page}) async {
    // final String _baseUrl = '$apiUri/member-info/my/profile';
    final String _baseUrl = 'http://localhost:8084/v1/pt-ticket/list?page={page}';


    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(_baseUrl.replaceAll('{page}', page.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return CalculateItemResult.fromJson(response.data);
  }
}