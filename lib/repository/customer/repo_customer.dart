import 'package:dio/dio.dart';
import 'package:jo_gym_app/functions/token_lib.dart';
import 'package:jo_gym_app/model/common_result.dart';
import 'package:jo_gym_app/model/customer/customer_create_request.dart';
import 'package:jo_gym_app/model/customer/customer_item_result.dart';
import 'package:jo_gym_app/model/customer/customer_response_result.dart';
import 'package:jo_gym_app/model/customer/customer_update_request.dart';

class RepoCustomer {
  Future<CommonResult> doCreate(CustomerCreateRequest request) async {
    // const String baseUrl = '$apiUri/customer/data';
     const String baseUrl = 'http://localhost:8083/v1/customer/data';


    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.post(baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return CommonResult.fromJson(response.data);
  }

  Future<CustomerItemResult> getList({required int page}) async {
    // final String _baseUrl = '$apiUri/member-info/my/profile';
    final String _baseUrl = 'http://localhost:8083/v1/customer/list?page={page}';


    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(_baseUrl.replaceAll('{page}', page.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return CustomerItemResult.fromJson(response.data);
  }


  Future<CustomerResponseResult> getData({required int customerId}) async {
    // final String _baseUrl = '$apiUri/member-info/my/profile';
    final String _baseUrl = 'http://localhost:8083/v1/customer/{customerId}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(_baseUrl.replaceAll('{customerId}', customerId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return CustomerResponseResult.fromJson(response.data);
  }


  Future<CommonResult> putData({required int customerId, required CustomerUpdateRequest request }) async {
    // final String _baseUrl = '$apiUri/member-info/my/profile';
    final String _baseUrl = 'http://localhost:8083/v1/customer/{customerId}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(_baseUrl.replaceAll('{customerId}', customerId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return CommonResult.fromJson(response.data);
  }



}
