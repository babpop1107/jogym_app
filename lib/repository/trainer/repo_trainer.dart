import 'package:dio/dio.dart';
import 'package:jo_gym_app/functions/token_lib.dart';
import 'package:jo_gym_app/model/common_result.dart';
import 'package:jo_gym_app/model/trainer/trainer_create_request.dart';
import 'package:jo_gym_app/model/trainer/trainer_item_result.dart';
import 'package:jo_gym_app/model/trainer/trainer_response_result.dart';
import 'package:jo_gym_app/model/trainer/trainer_update_request.dart';

class RepoTrainer {
  Future<CommonResult> doCreate(TrainerCreateRequest request) async {
    // const String baseUrl = '$apiUri/customer/data';
     const String baseUrl = 'http://localhost:8085/v1/trainer-member/data';


    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.post(baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return CommonResult.fromJson(response.data);
  }

  Future<TrainerItemResult> getList({required int page}) async {
    // final String _baseUrl = '$apiUri/member-info/my/profile';
    final String _baseUrl = 'http://localhost:8085/v1/trainer-member/list?page={page}';


    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(_baseUrl.replaceAll('{page}', page.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return TrainerItemResult.fromJson(response.data);
  }


  Future<TrainerResponseResult> getData({required int trainerMemberId}) async {
    // final String _baseUrl = '$apiUri/member-info/my/profile';
    final String _baseUrl = 'http://localhost:8085/v1/trainer-member/{trainerMemberId}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(_baseUrl.replaceAll('{trainerMemberId}', trainerMemberId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return TrainerResponseResult.fromJson(response.data);
  }


  Future<CommonResult> putData({required int trainerMemberId, required TrainerUpdateRequest request }) async {
    // final String _baseUrl = '$apiUri/member-info/my/profile';
    final String _baseUrl = 'http://localhost:8085/v1/trainer-member/{trainerMemberId}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(_baseUrl.replaceAll('{trainerMemberId}', trainerMemberId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return CommonResult.fromJson(response.data);
  }



}
