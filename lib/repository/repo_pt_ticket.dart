import 'package:dio/dio.dart';
import 'package:jo_gym_app/functions/token_lib.dart';
import 'package:jo_gym_app/model/common_result.dart';
import 'package:jo_gym_app/model/customer/customer_item_result.dart';
import 'package:jo_gym_app/model/pt/pt_ticket_create_request.dart';
import 'package:jo_gym_app/model/pt/pt_ticket_item_result.dart';
import 'package:jo_gym_app/model/pt/pt_ticket_update_request.dart';

class RepoPtTicket {
  Future<CommonResult> doCreate({required int trainerId, required PtTicketCreateRequest request}) async {
    // const String baseUrl = '$apiUri/customer/data';
    const String baseUrl = 'http://localhost:8084/v1/pt-ticket/trainer-id/{id}';


    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.post(baseUrl.replaceAll("{id}", trainerId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return CommonResult.fromJson(response.data);
  }


  Future<PtTicketItemResult> getList({required int page}) async {
    // final String _baseUrl = '$apiUri/member-info/my/profile';
    final String _baseUrl = 'http://localhost:8084/v1/pt-ticket/list?page={page}';


    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(_baseUrl.replaceAll('{page}', page.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return PtTicketItemResult.fromJson(response.data);
  }

  Future<CommonResult> putData({required int ptTicketId, required PtTicketUpdateRequest request }) async {
    // final String _baseUrl = '$apiUri/member-info/my/profile';
    final String _baseUrl = 'http://localhost:8084/v1/pt-ticket/{id}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(_baseUrl.replaceAll('{id}', ptTicketId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> doPtTicketDelete(int id) async {
    const String baseUrl = 'http://localhost:8084/v1/pt-ticket/pt-ticket-id/{ptTicketId}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(baseUrl.replaceAll('{ptTicketId}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }
  }